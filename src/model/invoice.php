<?php

namespace FinanceMobile;

class Invoice {
  var $transactionId;
  var $buyerPhone;
  var $buyerTin;
  var $cashierName;
  var $cashierTin;
  var $description;
  var $sellerName;
  var $sellerTin;
  private $_grossAmount;
  private $_netAmount;
  private $_totalDiscountAmount;
  private $_totalTaxAmount;
  private $_transactions = [];
  var $invoiceDate;
  
  function __constructor() {
  }

  function getGrossAmount() {
    return $this->_grossAmount;
  }
  function getNetAmount() {
    return $this->_netAmount;
  }
  function getTotalDiscountAmount() {
    return $this->_totalDiscountAmount;
  }
  function getTotalTaxAmount() {
    return $this->_totalTaxAmount;
  }
  function getTotalAmount() {
    return ($this->_grossAmount + $this->_totalTaxAmount) - $this->_totalDiscountAmount;
  }
  function getTransactions() {
    return $this->_transactions;
  }

  function clearTransactions() {
    $this->_transactions = [];
    $this->calcTotals();
  }

  function addTransactions(array $transactions) {
    array_push($this->_transactions, ...$transactions);
    $this->calcTotals();
  }
  
  function addTransaction($transaction) {
    array_push($this->_transactions, $transaction);
    $this->calcTotals();
  }

  function calcTotals() {
    $this->_totalTaxAmount = 0;
    $this->_totalDiscountAmount = 0;
    $this->_grossAmount = 0;
    $this->_netAmount = 0;

    foreach($this->_transactions as $item) {
      // discounts
      $this->_totalDiscountAmount += $item->discountAmount;

      // tax
      $this->_totalTaxAmount += $item->taxAmount;

      // totals
      $this->_grossAmount += $item->grossAmount;
      $this->_netAmount += $item->netAmount;
    }
  }

  function toJson() {
    return json_encode([
      'id' => null,
      'tenantId' => null,
      'transactionId' => $this->transactionId,
      'description' => $this->description,
      'buyerPhone' => $this->buyerPhone,
      'buyerTin' => $this->buyerTin,
      'cashierName' => $this->cashierName,
      'cashierTin' => $this->cashierTin,
      'sellerName' => $this->sellerName,
      'sellerTin' => $this->sellerTin,
      'grossAmount' => $this->_grossAmount,
      'netAmount' => $this->_netAmount,
      'totalDiscountAmount' => $this->_totalDiscountAmount,
      'totalTaxAmount' => $this->_totalTaxAmount,
      'transactionItems' => $this->_transactions,
      'invoiceDate' => date('y-m-d H:i:s', strtotime($this->invoiceDate)),
      'createdAt' => null,
      'updatedAt' => null,
    ]);
  }

}

class TransactionItem {
  var $itemReference;
  var $name;
  var $description;
  var $quantity;
  var $unitPrice;
  var $discountAmount;
  var $appliedTax;

  /// total price of the item, that is, price x quantity
  function getGrossAmount() {
    return ($this->unitPrice * $this->quantity);
  }
  
  /// total tax amount, based on the [discountedGrossAmount]
  function getTaxAmount() {
    return 15.8;
    // TODO
    // return (taxes != null ? taxes.totalTax(discountedGrossAmount) : 0);
  }

  function getDiscountedGrossAmount() {
    return ($this->grossAmount - $this->discountAmount);
  }

  function getNetAmount() {
    return ($this->discountedGrossAmount + $this->taxAmount);
  }

  function toJson() {
    return json_encode([
      'itemReference' => $this->itemReference,
      'name' => $this->name,
      'description' => $this->description,
      'quantity' => $this->quantity,
      'unitPrice' => $this->unitPrice,
      'grossAmount' => $this->grossAmount,
      'netAmount' => $this->netAmount,
      'discountAmount' => $this->discountAmount,
      'appliedTax' => $this->appliedTax,
    ]);
  }

}