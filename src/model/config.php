<?php

class Config {
  var $clientKey;
  var $encryptionKey;

  var $_schema = [
    'clientKey' => 'string',
    'encryptionKey' => 'string',
  ];

  // take the data from a config file and check against the pre-defined schema
  static function validateConfig($data) {
    // TODO
    // $jsonSchema = JsonSchema.createSchema(_schema);
    // return jsonSchema.validate(data);
  }
}

class CreateInvoice {
  private $data;

  function __constructor($tokenData) {
    $this->data = $tokenData;
  }

  function toJson() {
    return json_encode([
      'data' => $this->data
    ]);
  }
}