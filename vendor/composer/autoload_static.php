<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit01633824b00fd8a4511be605f86ac198
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'TaxQR\\' => 6,
        ),
        'L' => 
        array (
            'Lcobucci\\JWT\\' => 13,
            'Lcobucci\\Clock\\' => 15,
        ),
        'J' => 
        array (
            'JsonSchema\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'TaxQR\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
        'Lcobucci\\JWT\\' => 
        array (
            0 => __DIR__ . '/..' . '/lcobucci/jwt/src',
        ),
        'Lcobucci\\Clock\\' => 
        array (
            0 => __DIR__ . '/..' . '/lcobucci/clock/src',
        ),
        'JsonSchema\\' => 
        array (
            0 => __DIR__ . '/..' . '/justinrainbow/json-schema/src/JsonSchema',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit01633824b00fd8a4511be605f86ac198::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit01633824b00fd8a4511be605f86ac198::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
